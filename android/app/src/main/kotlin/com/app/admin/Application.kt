//package com.app.admin
//
//import android.os.Bundle
//
//import io.flutter.app.FlutterApplication
//import io.flutter.plugin.common.PluginRegistery
//import io.flutter.plugin.common.PluginRegistery.PluginRegistrantCallback
//import io.flutter.plugins.GeneratedPluginRegistrant
////import io.flutter.plugins.firebase.messaging.FlutterFirebaseMessagingBackgroundService
//import io.flutter.plugins.firebase.messaging.FlutterFirebaseMessagingService
//
//
//class Application : FlutterApplication(), PluginRegistrantCallback {
//
//    override fun onCreate() {
//        super.onCreate();
//        FlutterFirebaseMessagingService.setPluginRegistrant(this);
//    }
//
//    override fun registerWith(registery: PluginRegistery?) {
//        registery?.registrarFor("io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin");
//    }
//}