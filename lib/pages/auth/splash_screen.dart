import 'package:admin/pages/main_pages/home_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'login_page.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    loadData();
  }

  void loadData() async {
    Future.delayed(Duration(milliseconds: 700), (){
      ///Get value and navigate
      bool isSignedIn = FirebaseAuth.instance.currentUser != null;
      if(isSignedIn) {
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => HomePage()));
        // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => WelcomePage()));
      } else {
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => LoginPage()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(CupertinoIcons.settings, color: Colors.white,),

            SizedBox(height: 20),


            SizedBox(
              height: 35,
              child: CircularProgressIndicator(),
            )
          ],
        ),
      ),
    );
  }
}
