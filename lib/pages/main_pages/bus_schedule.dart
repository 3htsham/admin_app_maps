import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import '../../global_variables.dart';
import '../../repository.dart';
import 'pdf_viewer.dart';

class BusScheduleWidget extends StatefulWidget {
  const BusScheduleWidget({Key key}) : super(key: key);

  @override
  _BusScheduleWidgetState createState() => _BusScheduleWidgetState();
}

class _BusScheduleWidgetState extends State<BusScheduleWidget> {

  String filename = "newSchedule";
  String scheduleFileUrl = '';

  @override
  initState() {
    super.initState();
    getScheduleFile();
  }

  getScheduleFile() async {
    final ref = await FirebaseStorage.instance.ref('schedule').list(ListOptions(maxResults: 1));
    if(ref != null && ref.items.length > 0) {
      final firstRef = ref.items.first;
      final downloadUrl = await firstRef.getDownloadURL();
      print("File path");
      print(downloadUrl);
      setState(() {
        this.scheduleFileUrl = downloadUrl;
      });
    }
  }


  checkPermission() async {
    //1. Check Permission
    //1.2 If not granted, move back
    //1.3 If granted, pick file and upload
    var status = await Permission.storage.status;
    if(status == PermissionStatus.granted) {
      //If granted, pick file and upload
      pickAndUpload();
    } else {
      status  = await Permission.storage.request();
      if(status == PermissionStatus.granted) {
        //If granted, pick file and upload
        pickAndUpload();
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text("You must allow storage permission to upload schedule"),
              behavior: SnackBarBehavior.floating,
            )
        );
      }
    }
  }

  pickAndUpload() async {
    //Pick File and Upload
    FilePickerResult result = await FilePicker.platform.pickFiles(
      allowMultiple: false,
      type: FileType.custom,
      allowedExtensions: ['pdf'],
    );
    if (result != null) {
      File file = File(result.files.first.path);

      try {
        await FirebaseStorage.instance
            .ref('schedule/$filename.pdf')
            .putFile(file);
        await notifyUser(GlobalVariables.riderNotificationsTopic, "Schedule Updated", "Busses Schedule has just been updated, Please review.");
        await notifyUser(GlobalVariables.driverNotificationsTopic, "Schedule Updated", "Busses Schedule has just been updated, Please review.");
        setState((){
          this.scheduleFileUrl = '';
        });
        getScheduleFile();
      } catch (e) {
        print(e);
      }
    }
  }

  notifyUser(String topic, String title, String body) async {
    //TODO: Fire FCM
    await sendNotification(
        topic,
        title: title,
        notificationBody: body
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Schedule Update"),
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: Icon(CupertinoIcons.back),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [

            scheduleFileUrl.length > 2
             ? ListTile(
              onTap: () async {
                //TODO: Tap to view pdf file
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => PdfViewerPage(fileLink: scheduleFileUrl,)));
              },
              title: Text("Existing Schedule"),
              subtitle: Text("Tap here to view existing busses schedule"),
              leading: Icon(CupertinoIcons.clock_solid, color: Colors.white,),
            ) : SizedBox(),

            ListTile(
              onTap: (){
                //TODO: Pick a pdf file and upload
                checkPermission();
              },
              title: Text("Add new Schedule"),
              subtitle: Text("Update or Add Busses routes schedule. Remember the existing schedule will be overwritten."),
              leading: Icon(CupertinoIcons.clock, color: Colors.white,),
            ),

          ],
        ),
      ),
    );
  }
}
