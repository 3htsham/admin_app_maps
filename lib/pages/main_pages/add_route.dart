import 'package:admin/models/bus_route.dart';
import 'package:admin/pages/main_pages/choose_location.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';
import 'package:location/location.dart' as l;


class AddRoute extends StatefulWidget {
  const AddRoute({Key key}) : super(key: key);

  @override
  _AddRouteState createState() => _AddRouteState();
}

class _AddRouteState extends State<AddRoute> {

  GlobalKey<FormState> formKey = GlobalKey();
  var uuid = Uuid();
  BusRoute route = BusRoute();

  bool isLoading = false;

  @override
  initState(){
    super.initState();
    _checkLocationPermission();
  }

  _validateForm() {
    ///0 - Check if some location selected
    ///1 - Validate form
    ///2 - Generate ID
    ///3 - Add To Firestore

    if(this.route.location != null) {
      //1
      if (this.formKey.currentState.validate()) {
        this.formKey.currentState.save();
        //2
        this.route.id = uuid.v1();
        setState(() {
          this.isLoading = true;
        });

        //3
        FirebaseFirestore firestore = FirebaseFirestore.instance;
        // Create a CollectionReference called buses that references the firestore collection
        CollectionReference routes = firestore.collection('routes');

        Map _routeMap = this.route.toMap();
        GeoPoint geoPoint = GeoPoint(
            this.route.location.latitude, this.route.location.longitude);

        _routeMap['location'] = geoPoint;

        print(_routeMap);

        ///For manual Doc Id
        routes.doc(this.route.id).set(_routeMap)
            .then((value) {
          setState(() {
            this.isLoading = false;
          });
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text("Added New Route")));
          Navigator.of(context).pop();
        })
            .catchError((error) {
          setState(() {
            this.isLoading = false;
          });
          print("Failed to add route: $error");
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text("Error adding route")));
        });
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("Please choose route location")));
    }
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Scaffold(
      appBar: AppBar(
        title: Text('Add new Route'),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Form(
            key: this.formKey,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  SizedBox(height: 20,),

                  TextFormField(
                    keyboardType: TextInputType.text,
                    validator: (value) => value != null && value.length > 0 ? null : "Route name can't be empty",
                    style: Theme.of(context).textTheme.bodyText1,
                    decoration: InputDecoration(
                        hintText: "Route Name",
                        hintStyle: Theme.of(context).textTheme.bodyText1,
                        border: OutlineInputBorder()
                    ),
                    onSaved: (value) {
                      this.route.name = value;
                    },
                  ),

                  SizedBox(
                    height: 15,
                  ),


                  InkWell(
                    onTap: (){
                      //TODO: Navigate To Location Selector
                      Navigator.of(context).push(MaterialPageRoute(builder: (_) => ChooseLocation())).then((value) {
                        if(value != null) {
                          if(value is LocationModel) {
                            setState(() {
                              this.route.location = value;
                            });
                          }
                        } else {
                          print('Parameter value is: null');
                        }
                      });
                    },
                    child: Container(
                      // height: 30,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        border: Border.all(color: Theme.of(context).textTheme.caption.color, width: 1),
                        color: Colors.transparent
                      ),
                      child: Row(
                        children: [
                          IconButton(icon: Icon(CupertinoIcons.location_solid), onPressed: (){}),
                          this.route.location == null
                              ? Text('Select Location')
                          : Flexible(
                            child: FittedBox(
                              child: Text('Latitude: ${this.route.location.latitude}, '
                                  'Longitude: ${this.route.location.longitude}',
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                            ),
                          ),
                          // Spacer(),
                          // IconButton(icon: Icon(CupertinoIcons.forward), onPressed: (){},),
                        ],
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 15,
                  ),

                  this.isLoading
                      ? Center(child: SizedBox(height: 35, child: CircularProgressIndicator()))
                      : ElevatedButton(
                    onPressed: () {
                      _validateForm();
                    },
                    style: ElevatedButton.styleFrom(shape: StadiumBorder()),
                    child: Center(
                      child: Text("Add Route"),
                    ),
                  ),



                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _checkLocationPermission() async {
    if(await getLocationPermission()) {

    } else {
      Navigator.of(context).pop();
    }
  }

  Future<bool> getLocationPermission() async {
    if(await l.Location.instance.hasPermission() == l.PermissionStatus.granted) {
      ///Granted
      ///We can Update location
      return true;
    } else {
      final _req = await l.Location().requestPermission();
      if(_req == l.PermissionStatus.granted) {
        ///Granted
        ///We can Update location
        return true;
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
                content: Text("You must allow permission to update location")
            )
        );
        return false;
      }
    }
  }
}
