import 'package:admin/pages/auth/login_page.dart';
import 'package:admin/pages/main_pages/bus_schedule.dart';
import 'package:admin/pages/main_pages/send_notification.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'add_bus.dart';
import 'add_route.dart';


class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {


  _logout() async {
    ///Set prefs to loggedOut
    ///Clear the Activity Stack
    ///Navigate to Login page
    await FirebaseAuth.instance.signOut();

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LoginPage()), (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome'),
        actions: [
          TextButton(
              onPressed: () {
                _logout();
              },
              child: Text(
                "Logout",
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(color: Colors.black),
              ))
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [

              Container(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [

                    Expanded(child: ElevatedButton(onPressed: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddBusPage()));
                    }, child: Text('Add bus'))),

                    SizedBox(width: 10,),

                    Expanded(child: ElevatedButton(onPressed: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddRoute()));
                    }, child: Text('Add Route')))


                  ],
                ),
              ),

              ListTile(
                onTap: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => SendNotificationWidget()));
                  },
                title: Text("Notify Users"),
                subtitle: Text("Send notifications to user by using custom title and message"),
                leading: Icon(CupertinoIcons.bell_solid, color: Colors.white,),
              ),

              ListTile(
                onTap: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => BusScheduleWidget()));
                },
                title: Text("Buses Schedule"),
                subtitle: Text("Update or Add Busses routes schedule"),
                leading: Icon(CupertinoIcons.clock, color: Colors.white,),
              ),


            ],
          ),
        ),
      ),
    );
  }
}
