import 'package:admin/models/bus.dart';
import 'package:admin/models/bus_route.dart';
import 'package:admin/models/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class AddBusPage extends StatefulWidget {
  const AddBusPage({Key key}) : super(key: key);

  @override
  _AddBusPageState createState() => _AddBusPageState();
}

class _AddBusPageState extends State<AddBusPage> {

  Bus bus = Bus();
  GlobalKey<FormState> formKey = GlobalKey();
  var uuid = Uuid();
  
  
  
  List<MyUser> _drivers = [];
  MyUser _selectedDriver = MyUser();

  List<BusRoute> _routes = [];
  List<BusRoute> _selectedRoutes = [];

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    _getExistingDrivers();
    _getRoutes();
  }

  _validateForm() {
    ///1 - Validate form
    ///2 - Check if any driver selected
    ///3 - Check if at least one route is selected

    //1
    if(this.formKey.currentState.validate()) {
      this.formKey.currentState.save();
      //2
      if(this._selectedDriver != null && this._selectedDriver.id != null) {
        //If driver selected
        this.bus.driverId = this._selectedDriver.id;
        //3
        this.bus.stops.clear();
        if(this._selectedRoutes.isNotEmpty) {
          //if at least one route selected
          this._selectedRoutes.forEach((_route) {
            this.bus.stops.add(_route.id);
          });

          //TODO: Add bus to firestore
          _addBus();

        } else {
          //if no route selected
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("You must select at least one route")));
        }
      } else {
        //If no driver selected
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Please select a driver")));
      }
    }
  }

  void _addBus() async {
    setState((){
      this.isLoading = true;
    });
    this.bus.id = uuid.v1();
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    // Create a CollectionReference called buses that references the firestore collection
    CollectionReference buses = firestore.collection('buses');

    print(this.bus.toMap());
    ///For manual Doc Id
    buses.doc(this.bus.id).set(this.bus.toMap())
        .then((value) {
      setState((){
        this.isLoading = false;
      });
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Added New Bus")));
      Navigator.of(context).pop();
    })
        .catchError((error) {
      setState((){
        this.isLoading = false;
      });
      print("Failed to add bus: $error");
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Error adding Bus")));
    });

  }
  

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Scaffold(
      appBar: AppBar(
        title: Text('Add new Bus'),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Form(
            key: this.formKey,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  SizedBox(height: 20,),

                  TextFormField(
                    keyboardType: TextInputType.number,
                    validator: (value) => value != null && value.length > 0 ? null : "Bus number can't be empty",
                    style: Theme.of(context).textTheme.bodyText1,
                    decoration: InputDecoration(
                        hintText: "Bus No.",
                        hintStyle: Theme.of(context).textTheme.bodyText1,
                        border: OutlineInputBorder()
                    ),
                    onSaved: (value) {
                      this.bus.busNo = value;
                    },
                  ),

                  SizedBox(height: 15,),


                  Text('Select a driver from list below', style: textTheme.bodyText1,),
                  SizedBox(height: 5),
                  Container(
                    height: 150,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white, width: 1),
                      borderRadius: BorderRadius.circular(3)
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [

                          _drivers.isEmpty
                          ? SizedBox()
                              : ListView.builder(
                            primary: false,
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            itemCount: _drivers.length,
                            itemBuilder: (context, index) {
                              return ListTile(
                                title: Text('${_drivers[index].name}', style: textTheme.bodyText2,),
                                trailing: myCheckbox(this._selectedDriver.id == _drivers[index].id),
                                onTap: (){
                                  setState(() {
                                    this._selectedDriver = _drivers[index];
                                  });
                                },
                              );
                            },
                          )

                        ],
                      ),
                    ),
                  ),

                  SizedBox(height: 15,),


                  Text('Select bus routes from list below', style: textTheme.bodyText1,),
                  SizedBox(height: 5),
                  Container(
                    height: 150,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 1),
                        borderRadius: BorderRadius.circular(3)
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [

                          _routes.isEmpty
                              ? SizedBox()
                              : ListView.builder(
                            primary: false,
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            itemCount: _routes.length,
                            itemBuilder: (context, index) {

                              final _isInSelected = _selectedRoutes.where((_route) => _route.id == _routes[index].id);

                              return ListTile(
                                title: Text('${_routes[index].name}', style: textTheme.bodyText2,),
                                trailing: myCheckbox(_isInSelected.isNotEmpty),
                                onTap: (){
                                  if(_isInSelected.isEmpty) {
                                    ///Add to selected Routes
                                    setState(() {
                                      this._selectedRoutes.add(_routes[index]);
                                    });
                                  } else {
                                    ///Remove from selected Routes
                                    setState(() {
                                      this._selectedRoutes.removeWhere((_route) => _route.id == _routes[index].id);
                                    });
                                  }
                                },
                              );
                            },
                          )

                        ],
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 15,
                  ),

                  this.isLoading
                      ? Center(child: SizedBox(height: 35, child: CircularProgressIndicator()))
                      : ElevatedButton(
                    onPressed: () {
                      _validateForm();
                    },
                    style: ElevatedButton.styleFrom(shape: StadiumBorder()),
                    child: Center(
                      child: Text("Add Bus"),
                    ),
                  ),



                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget myCheckbox(bool isChecked) {
    return Container(
      height: 20,
      width: 20,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2),
          border: Border.all(color: Colors.green, width: 0.8),
          color: isChecked ? Colors.green : Colors.transparent
      ),
      child: Center(
        child: isChecked ? Icon(
          CupertinoIcons.checkmark_alt,
          color: Colors.white,
          size: 14,
        ) : SizedBox(),
      ),
    );
  }



  _getExistingDrivers() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference users = firestore.collection('users');
    final ref = users.where('isDriver', isEqualTo: true);
    ref.get().then((snapshot) {
      if(snapshot.docs.isNotEmpty) {
        final docs = snapshot.docs;
        docs.forEach((docSnap) {
          final _drv = MyUser.fromJson(docSnap.data());
          setState(() {
            _drivers.add(_drv);
          });
        });
      }
    });
  }

  _getRoutes() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    CollectionReference routesRef = firestore.collection('routes');
    routesRef.get().then((snapshot) {
      if(snapshot.docs.isNotEmpty) {
        final docs = snapshot.docs;
        docs.forEach((docSnap) {
          final _route = BusRoute.fromJson(docSnap.data());
          setState(() {
            _routes.add(_route);
          });
        });
      }
    });
  }

  
}










