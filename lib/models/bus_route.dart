class BusRoute {

  var id;
  var name;

  LocationModel location;

  BusRoute();

  BusRoute.fromJson(Map<String, dynamic> data){
    this.id = data['id'];
    this.name = data['name'];
  }

  Map<String, dynamic> toMap(){
    Map<String, dynamic> data = Map();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }

}

class LocationModel {
  var latitude;
  var longitude;

  LocationModel({this.latitude, this.longitude});

}