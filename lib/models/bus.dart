class Bus {

  var id;
  var busNo;
  var driverId;
  List<String> stops = [];

  Bus();

  Bus.fromJson(Map<String, dynamic> data) {
    id = data['busId'];
    busNo = data['busNo'];
    driverId = data['driverId'];
    if(data['stops'] != null) {
      data['stops'].forEach((item){
        this.stops.add(item);
      });
    }
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> data = Map();
    data['busId'] = this.id;
    data['busNo'] = this.busNo;
    data['driverId'] = this.driverId;
    data['stops'] = this.stops != null ? this.stops.map((e) => e).toList() : [];
    // var list = this.stops.map((e) => e); ['1', '2', '3']
    return data;
  }

}