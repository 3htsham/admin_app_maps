import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'pages/auth/splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          scaffoldBackgroundColor: Colors.white38,
          primarySwatch: Colors.blue,
          accentColor: Colors.orange,
          primaryColor: Colors.white,
          textTheme: TextTheme(
            subtitle1: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),
            subtitle2: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),
            bodyText1: TextStyle(fontSize: 14, fontWeight: FontWeight.normal, color: Colors.white),
            bodyText2: TextStyle(fontSize: 12, fontWeight: FontWeight.normal, color: Colors.white),
            caption: TextStyle(fontSize: 10, fontWeight: FontWeight.normal, color: Colors.white),
          )
      ),
      home: SplashScreen(),
    );
  }
}
